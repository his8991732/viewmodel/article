import { Article } from "./article";
export interface IArticleService {

    getArticles(): Promise<Article[]>;

    removeArticle(id: number): any;
        
    modifyArticle(article: Article): any;

}